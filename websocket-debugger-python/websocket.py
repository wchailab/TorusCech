#   Copyright (C) 2018 Wei Chai
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import tornado.ioloop
import tornado.web
import tornado.websocket
import json
import numpy
np = numpy
import matplotlib.pyplot as plt

import threading
import _thread
import queue

def get_radius(P):
    """
    Get circumcircle radius of P points, and the center
    P = np.array of shape (npoints, dim)
    """
    N, dim = P.shape
    D2 = np.sum(np.square(
        P.reshape((1, N, dim)) - P.reshape((N, 1, dim))
    ), axis=2)

    mat = np.empty((N+1, N+1))
    mat[0, 0] = 0
    mat[1:, 0] = mat[0, 1:] = 1
    mat[1:, 1:] = -D2/2

    x = np.linalg.inv(mat)[:, 0]
    return np.sqrt(x[0]), x[1:] @ P


class PlotThread():
    def __init__(self, q):
        # threading.Thread.__init__(self)
        self.q = q
        self.loop = True
        
    def init(self):
        plt.close()
        self.fig = plt.subplot()
        self.fig.hlines([0, 1], -.5, 1.5, linewidths=1)
        self.fig.vlines([0, 1], -.5, 1.5, linewidths=1)
        plt.ion()
        plt.show()
        
    def run(self):
        self.init()
        while self.loop:
            try:
                plt.pause(0.1)
                while not self.q.empty():
                    d = self.q.get_nowait()
                    if d['type'] == "points":
                        self.init()
                        self.plot_scatter(d['data'])
                    elif d['type'] == "simplex":
                        self.plot_triangle(d['data'])
                        # self.plot_circle(d['data'])
            except:
                print("Error...")
                import logging
                logging.exception("Error!")
                print("Error")
                break
    
    def plot_triangle(self, points):
        x, y = zip(*points)
        x = list(x)
        y = list(y)
        x.append(x[0])
        y.append(y[0])
        self.fig.plot(x, y)
        
    def plot_circle(self, points):
        points = numpy.array(points)
        r, c = get_radius(points)
        circle = plt.Circle(c, r, fill=False)
        self.fig.add_patch(circle)
    
    def plot_scatter(self, points):
        x, y = zip(*points)
        x = numpy.array(x)
        y = numpy.array(y)
        self.fig.scatter(x, y, marker=".", c='r')
        self.fig.scatter(x-1, y, marker=".", c='b')
        self.fig.scatter(x, y-1, marker=".", c='b')  
        self.fig.scatter(x-1, y-1, marker=".", c='b')

class PlotServer(tornado.websocket.WebSocketHandler):
    def initialize(self, q):
        self.q = q

    def check_origin(self, origin):
        return True
    def open(self):
        print("Opened")
    def on_message(self, message):
        # print("Message")
        try:
            d = json.loads(message)
        except:
            print("Json Error!")
        self.q.put(d)
    def on_close(self):
        print("Closed")
        

class TornadoThread(threading.Thread):
    def __init__(self, q):
        threading.Thread.__init__(self)
        self.q = q
    def run(self):
        ioloop = self.ioloop = tornado.ioloop.IOLoop.current()
        application = tornado.web.Application([
            (r"/", PlotServer, {"q": self.q}),
        ])
        application.listen(8998)
        
        # tornado.ioloop.PeriodicCallback(lambda: None, 1).start()
        ioloop.start()

        
if __name__ == "__main__":
    q = queue.Queue()
    thd = TornadoThread(q)
    thd.start()
    
    plot = PlotThread(q)
    plot.run()
    
    thd.ioloop.add_callback(
        lambda: thd.ioloop.stop()
    )