# [TorusCech](https://gitlab.com/wchailab/TorusCech)
## Computing persistent Čech homology on an `n`-D flat torus

* `websocket-debugger-python`:
    - A simple websocket server to plot `2`-D Delaunay triangluation in real time.

[![](https://beacon-c.appspot.com/gitlab/TorusCech/websocket-debugger-python?name)](https://gitlab.com/wchailab/TorusCech)