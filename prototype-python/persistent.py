#   Copyright (C) 2018 Wei Chai
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging

import numpy as np

import scipy.spatial
import scipy.sparse
import scipy.sparse.linalg
import numba

from timerlogger import TimerLogger

EPS = 1e-5

logger = logging.getLogger('persist')


# noinspection PyPep8Naming
@numba.jit(nopython=True, cache=True)
def get_radius(P):
    """
    Get circumcircle radius of P points, and the center
    P = np.array of shape (npoints, dim)
    """
    N, dim = P.shape
    D2 = np.sum(np.square(
        P.reshape((1, N, dim)) - P.reshape((N, 1, dim))
    ), axis=2)

    mat = np.empty((N+1, N+1))
    mat[0, 0] = 0
    mat[1:, 0] = mat[0, 1:] = 1
    mat[1:, 1:] = -D2/2

    x = np.linalg.inv(mat)[:, 0]
    return np.sqrt(x[0]), x[1:] @ P


def get_lower(simplices, dim):
    """
    ASSUME simplices are sorted
    input: `dim`-dimensional simplices given by indices; should be tuples
    returns: `(dim-1)`-dimensional simplices, boundary map, boundary map signs
    boundary map is given by a list of parents
    """
    # assert all(type(s) is tuple for s in simplices), "simplices should be tuples"
    lowers = []  # list of lower dim simplices
    parents = []  # list of parents of each lower dim simplices
    signs = []

    dparents = {}
    dsigns = {}

    log = TimerLogger(delay=5)
    logger.info(f'get_lower for {len(simplices)} in dim {dim}')

    for iparent, parent in enumerate(simplices):
        sign = 1
        for i in range(dim+1):
            lower = parent[:i] + parent[i+1:]

            dparents.setdefault(lower, []).append(iparent)
            dsigns.setdefault(lower, []).append(sign)

            # flip sign
            sign = -sign
        # notify
        log.log(iparent)

    for k in dparents:
        lowers.append(k)
        parents.append(dparents[k])
        signs.append(dsigns[k])

    return lowers, parents, signs


def is_critical_pt(simplex, points, kdtree):
    """
    simplex: indices in `points`
    returns: True/False, radius
    """
    P = points[simplex,]
    r, center = get_radius(P)
    _, ind = kdtree.query(center)
    return ind in simplex, r


def get_values(simplices, parents, parent_values, points, kdtree):
    values = []
    log = TimerLogger(delay=5)
    logger.info(f'get_values for {len(simplices)} in dim {len(simplices[0])-1}')

    for isimp, simplex in enumerate(simplices):
        critical, r = is_critical_pt(simplex, points, kdtree)
        if critical:
            values.append(r)
        else:
            values.append(min(parent_values[i] for i in parents[isimp]))
        
        log.log(isimp)
    return values


def get_boundary_matrix(parents, signs):
    assert len(parents) == len(signs)
    data = []
    rows = []
    cols = []
    log = TimerLogger(delay=5)
    logger.info(f'get_boundary_matrix for {len(parents)}')
    for i in range(len(parents)):
        for parent, sign in zip(parents[i], signs[i]):
            data.append(sign)
            rows.append(i)
            cols.append(parent)
            
        log.log(i)
    return scipy.sparse.coo_matrix((data, (rows, cols)))


def reorder_matrix(m, row_orders, col_orders):
    """
    Assume `m` is a *coo* sparse matrix
    Reorder the columns and rows of a matrix according to some orders
    Usually, col/row_orders are given by argsort
    """
    m.col = np.argsort(col_orders)[m.col]
    m.row = np.argsort(row_orders)[m.row]
    return m


def sort_boundary_map(boundary, value_low, value_high):
    return reorder_matrix(boundary, np.argsort(value_low), np.argsort(value_high))


def to_F2(data):
    return data != 0

@numba.jit(nopython=True, cache=True)
def _xor_merge(l1, l2):
    """
    Assume lists are sorted and no repetition occurs
    """
    l = []
    i1, i2 = 0, 0
    while True:
        if i1 >= len(l1):
            l.extend(l2[i2:])
            break
        elif i2 >= len(l2):
            l.extend(l1[i1:])
            break
        elif l1[i1] == l2[i2]:
            i1 += 1
            i2 += 1
        elif l1[i1] < l2[i2]:
            l.append(l1[i1])
            i1 += 1
        elif l1[i1] > l2[i2]:
            l.append(l2[i2])
            i2 += 1
    return l

# @numba.jit
def barcode_F2(m):
    """
    Assume `m` is a sparse matrix (easy to get columns) in $F_2$
    return: bars (list of (birth, death)), new cycles (list of birth), echelon form
    """
    log = TimerLogger(delay=5)
    logger.info(f'barcode_F2 for matrix of {m.shape}')

    bars = []
    new_cycles = []
    m = m.T.tolil()
    ncol, nrow = m.shape  # `m` is transpose
    # lowest non-zero entry in each col
    low = np.full((nrow,), -1)

    for c in range(ncol):
        # notify
        log.log(c)

        while True:
            indices = m.rows[c]
            if not indices:
                # empty column => new cycle
                new_cycles.append(c)
                break
            if low[indices[-1]] < 0:
                # new death at c
                bars.append((indices[-1], c))
                low[indices[-1]] = c
                break
            # m[c] = (m[c] != m[low[indices[-1]]])
            m.rows[c] = _xor_merge(m.rows[c], m.rows[low[indices[-1]]])

    m.data = [[True]*len(r) for r in m.rows]

    return bars, new_cycles, m.tocoo().T


