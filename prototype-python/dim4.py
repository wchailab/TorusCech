#   Copyright (C) 2018 Wei Chai
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import pathlib
import pickle
import logging

import numpy as np
import scipy.spatial
import scipy.sparse
import scipy.sparse.linalg
import numba

import persistent

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)-5s %(message)s',
    datefmt='%H:%M'
)
logger = logging.getLogger('main')

NAME = 'dim4-10k'
SAVE_TO = pathlib.Path(f'./data/{NAME}')
if not SAVE_TO.exists():
    SAVE_TO.mkdir()

dim = 4
N = 10000

points = np.random.random((N, dim))

kdtree = scipy.spatial.cKDTree(points)

delaunay = scipy.spatial.Delaunay(points, qhull_options="Qbb Qc Qz")
logger.info(f'Simplices: {len(delaunay.simplices)}; coplanar : {len(delaunay.coplanar)}')

for i, s in enumerate(delaunay.simplices):
    if not persistent.is_critical_pt(s, points, kdtree)[0]:
        logging.info(f'Not critical: {s} / {i} \n {points[s]}')
        raise Exception('Not critical')
else:
    logging.info('All critical!')

np.savez(
    SAVE_TO / f'{NAME}.npz',
    points=points,
    delaunay_simplices=delaunay.simplices
)

simplices = [list(map(tuple, delaunay.simplices))]
parents = [[]]
values = [persistent.get_values(delaunay.simplices, [], [], points, kdtree)]
boundaries = []

for d in range(dim, 0, -1):
    s = simplices[-1]
    v = values[-1]

    logging.info(f'get_lower for dim {d}...')
    s2, p, signs = persistent.get_lower(s, dim=d)
    logging.info(f'get_values for dim {d}...')
    v2 = persistent.get_values(s2, p, v, points, kdtree)
    logging.info(f'get_boundary_matrix for dim {d}...')
    bm = persistent.get_boundary_matrix(p, signs)

    simplices.append(s2)
    parents.append(p)
    values.append(v2)
    boundaries.append(bm)
    
    pickle.dump(
        [s2, p, v2, bm],
        (SAVE_TO / f'{NAME}-spvb-dim-{d}.pickle').open('wb')
    )

    logging.info(f'Finish dim {d}!')

pickle.dump(
    [simplices, parents, values, boundaries],
    (SAVE_TO / f'{NAME}-spvb.pickle').open('wb')
)

bars = []

for i in range(dim):
    ordered_bm = persistent.sort_boundary_map(
        boundaries[i].copy(),
        values[i+1], values[i]
    )
    bar, nc, ech = persistent.barcode_F2(
        persistent.to_F2(ordered_bm)
    )

    pickle.dump(
        [bar, nc, ech],
        (SAVE_TO / f'{NAME}-bars-{dim-i}-{dim-i-1}.pickle').open('wb')
    )

    bars.append(bar)
    logging.info(f'{NAME}-bars-{dim-i}-{dim-i-1} is done!')

pickle.dump(
        bars,
        (SAVE_TO / f'{NAME}-bars.pickle').open('wb')
    )
