# [TorusCech](https://gitlab.com/wchailab/TorusCech)
## Computing persistent Čech homology on an `n`-D flat torus

* `prototype-python`:
    - Prototype of the algorithm in the case of a `n`-D cube in Python.

[![](https://beacon-c.appspot.com/gitlab/TorusCech/prototype-python?name)](https://gitlab.com/wchailab/TorusCech)