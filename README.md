# TorusCech
## Computing persistent Čech homology on an `n`-D flat torus

### Showcase
_Divide and conquer Delaunay triangulation with k-d tree on a `2d` flat torus_

![Delaunay Triangulation on a 2d torus](https://bit.ly/308CIeV)


_Illustration for the torus embedded in `3d` space_

![Embedded in 3 dimensional space](https://bit.ly/2RKM7Gy)
![Embedded in 3 dimensional space](https://bit.ly/2xkdHRB)


### Project Structure
* [`src`](https://gitlab.com/wchailab/TorusCech/blob/master/src/): C++ codes to do the computation
    - [`KDTree.h`](https://gitlab.com/wchailab/TorusCech/blob/master/src/KDTree.h): A general **kD tree** which applies to multiple types of queries.
    - `TorusKDTree.h`: An adaptation of kD tree to a flat torus.
    - `Predictors.h`: Classes for different types of queries in an kD tree. Each class has a function to evaluate at a single point as well as a prediction on a bounding box. Query types include nearest neighbors and more.
    - [`TorusDelaunay.h`](https://gitlab.com/wchailab/TorusCech/blob/master/src/TorusDelaunay.h): A **divide and conquer** algorithm to build Delaunay triangulation on a flat torus.
    - `BoundaryMap.h`: Construction of the Čech complex and the boundary maps based on Delaunay triangulation. Computation of the homology.
    - `Geometry.h`: Common geometric operations.
* [`websocket-debugger-python`](https://gitlab.com/wchailab/TorusCech/blob/master/websocket-debugger-python/):
    - A simple websocket server to plot `2`-D Delaunay triangluation in real time.
* [`prototype-python`](https://gitlab.com/wchailab/TorusCech/blob/master/prototype-python/):
    - Prototype of the algorithm in the case of a `n`-D cube in Python.

[![](https://beacon-c.appspot.com/gitlab/TorusCech?name)](https://gitlab.com/wchailab/TorusCech/blob/master/README.md)