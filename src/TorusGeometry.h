/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once

#include <array>
#include <utility>
#include "Geometry.h"


template <unsigned D> using Sheet = Matrix<int, D, 1>;
template <unsigned D, typename T> using Sheeted = pair<Sheet<D>, T>;


template <unsigned D>
class CoveringPoint {
public:
	CoveringPoint() : point(Point<D>::Zero()), sheet(Sheet<D>::Zero()) {};
	Sheet<D> sheet;
	Point<D> point;

	inline Point<D> toPoint() { return point + sheet; };
	// operator Point<D>&() { return point; }
};

inline double torus_dist(double a, double b) {
	double d = abs(a - b);
	return (d > .5) ? 1 - d : d;
}

template<unsigned D>
inline double torus_dist2(const Point<D> &p1, const Point<D> &p2) {
	double dist2;
	size_t pp1 = p1.data();
	size_t pp2 = p2.data();
	for (size_t i = 0; i < D; ++i) {
		dist2 += pow(torus_dist(*(pp1 + i), *(pp2 + i)));
	}
	return dist2;
}

template<unsigned D>
inline double torus_dist(const Point<D> &p1, const Point<D> &p2) {
	return sqrt(torus_dist2(p1, p2));
}