/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include "lib/json.hpp"

#include <string>
#include <vector>
#include <fstream>

#include "config.h"
#include "Geometry.h"
#include "TorusGeometry.h"
#include "BoundaryMap.h"

using namespace std;
using json = nlohmann::json;

template <unsigned D>
class Saver {
public:
	Saver(const Config& config)
		: fullpath(config.output_path + config.filename),
		writer(fullpath, ios::binary) {
	};

	void saveValues(const SimplexValues<D>& simplexvalues) {
		j["dim-" + to_string(simplexvalues.dimension)]["value"] = json(
			simplexvalues.values
		);
		j["dim-" + to_string(simplexvalues.dimension)]["critical"] = json(
			simplexvalues.critical
		);
	};

	void saveBars(
		const vector<array<size_t, 2>>& bars,
		unsigned int k
	) {
		j["persistent-" + to_string(k)]["bars"] = json::array();
		auto& jb = j["persistent-" + to_string(k)]["bars"];
		for (const array<size_t, 2>& b : bars) {
			jb.push_back({ b[0], b[1] });
		}
	}

	void dump() {
		writer << j;
	}
private:
	string fullpath;
	ofstream writer;
	json j;
};
