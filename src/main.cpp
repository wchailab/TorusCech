/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#include "constants.h"

#include <iostream>

#include "TorusDelaunay.h"
#include "save.h"
// #include "save.json.h"
#include "config.h"

using namespace std;


extern TorusDelaunay<DIM> testDelauney();  // test.cpp
extern void testHomology(TorusDelaunay<DIM>& delaunay, Saver<DIM> &saver);  // test.cpp

int main()
{
	srand(SEED);

	cout << "Saving to ";
	Config config;
	cout << config.output_path << config.filename << endl;
	Saver<DIM> saver(config.output_path, config.filename);

	TorusDelaunay<DIM> delaunay = testDelauney();
	saver.savePoints(*delaunay.points);
	testHomology(delaunay, saver);

	cout << "Done!" << endl;
	cin.get();
}