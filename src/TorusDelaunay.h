/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include "util.h"  // sorted_insert
#include "Geometry.h"
#include "TorusGeometry.h"
#include "KDTree.h"
#include "TorusKDTree.h"
#include "Predictors.h"
#include <iostream>
#include <queue>
#include <memory>
#include <utility>
#include <vector>
#include <array>
//#include <unordered_map>
#include <map>
#include <algorithm>


#ifdef WSDEBUG
#include "lib\json.hpp"
#include "lib\easywsclient.hpp"
#include "lib\easywsclient.cpp"
using json = nlohmann::json;
using easywsclient::WebSocket;
#define WSADDR "ws://127.0.0.1:8998/"
#endif // WSDEBUG

#ifdef TIMERLOGGER
#include "timerlogger.h"
extern TimerLogger timerlogger; // defined in timerlogger.cpp
#endif

template <unsigned D> class TorusDelaunay;

template <unsigned D>
struct CompareSheet {
	bool operator()(
		const typename TorusDelaunay<D>::SheetedSimplex &lhs, 
		const typename TorusDelaunay<D>::SheetedSimplex &rhs
	) const {
		return lexicographical_compare(
			lhs.begin(), lhs.end(),
			rhs.begin(), rhs.end(),
			[](const Sheeted<D, size_t> &lhs, const Sheeted<D, size_t> &rhs) {
				if (lhs.second == rhs.second) {
					return lexicographical_compare(
						lhs.first.data(), lhs.first.data() + lhs.first.size(),
						rhs.first.data(), rhs.first.data() + rhs.first.size()
					);
				}
				else {
					return lhs.second < rhs.second;
				}
			}
		);
	}
};

template <unsigned D>
void make_leading_zero(typename TorusDelaunay<D>::SheetedSimplex &face) {
	Sheet<D> first = face[0].first;
	for (Sheeted<D, size_t> &spoint : face) {
		spoint.first -= first;
	}
}


template <unsigned D>
class TorusDelaunay {
public:
	using Simplex = vector<size_t>;
	using SheetedSimplex = vector<Sheeted<D, size_t>>;
	using FaceMap = map<Simplex, bool>;
	using SheetedFaceMap = map<SheetedSimplex, bool, CompareSheet<D>>;

	TorusDelaunay(shared_ptr<const vector<Point<D>>> points);
	~TorusDelaunay() {};

	shared_ptr<const vector<Point<D>>> points;
	TorusKDTree<D> tkdtree;

	vector<Simplex> pureSimplices;
	// First sheet is 0.
	vector<SheetedSimplex> crossSimplices;

protected:
	// NOTE never used
	bool findFirstOnWall(Simplex& ret, const Box<D>& box);
	bool findFirstOnBoundary(SheetedSimplex& ret);

	bool growSimplex(Simplex &ret, const Box<D>& box);
	bool growSheetedSimplex(SheetedSimplex &ret);

	bool nextDelaunaySimplex(
		Simplex &ret,
		bool &orientation,
		const Simplex& face,
		const Box<D>& root
	);
	bool nextSheetedDelaunaySimplex(
		SheetedSimplex &ret,
		bool &orientation,
		const SheetedSimplex& face
	);

	void updateActiveSheetedFaces(
		const SheetedSimplex &simplex,
		bool orientation,  // of the simplex
		SheetedFaceMap &activeCrossFaces,
		FaceMap &activePureFaces
	);
	void classifySheetedFaces(
		const SheetedSimplex &face, bool orientation,
		SheetedFaceMap &activeCrossFaces,
		FaceMap &activePureFaces
	);
	void updateActiveFaces(
		const Simplex &simplex,
		bool orientation,  // of the simplex
		unsigned int axis, double wall,
		FaceMap &activeCrossFaces,
		array<FaceMap, 2> &activePureFaces
	);
	void classifyFaces(
		const typename FaceMap::value_type &facepair,
		unsigned int axis, double wall,
		FaceMap &activeCrossFaces,
		array<FaceMap, 2> &activePureFaces
	);

	void build(const Box<D>& box, FaceMap& activeFaces);
	void buildOnTorus();
private:
	size_t num_processed = 0;
	void buildOnPointBox(const Box<D>& box, FaceMap& activeFaces);

	template <typename T>
	inline vector<Point<D>> simplexPoints(const vector<T> &simplex) {
		return simplexPoints(simplex.begin(), simplex.end());
	};
	inline vector<Point<D>> simplexPoints(
		const typename Simplex::const_iterator& begin,
		const typename Simplex::const_iterator& end
	) {
		vector<Point<D>> ptface;
		for (auto i = begin; i != end; ++i) {
			ptface.push_back((*points)[*i]);
		}
		return ptface;
	};
	inline Point<D> castPoint(const Sheeted<D, size_t>& p) {
		return (*points)[p.second] + p.first.cast<double>();
	}
	inline vector<Point<D>> simplexPoints(
		const typename SheetedSimplex::const_iterator& begin,
		const typename SheetedSimplex::const_iterator& end
	) {
		vector<Point<D>> ptface;
		for (auto i = begin; i != end; ++i){
			ptface.push_back(castPoint(*i));
		}
		return ptface;
	};
#ifdef WSDEBUG
	unique_ptr<WebSocket> ws;
	void sendPoints() {
		if (ws == nullptr)
			return;
		json j;
		j["type"] = "points";
		j["data"] = json::array();

		for (const Point<D> &p : *points) {
			j["data"].push_back(vector<double>(p.data(), p.data() + p.size()));
		}

		ws->send(j.dump());
		ws->poll();
	};
	void sendSimplex(const Simplex &simplex) {
		if (ws == nullptr)
			return;
		json j;
		j["type"] = "simplex";
		j["data"] = json::array();

		for (size_t s : simplex) {
			Point<D> p = (*points)[s];
			j["data"].push_back(vector<double>(p.data(), p.data() + p.size()));
		}

		ws->send(j.dump());
		ws->poll();
	};
	void sendSheetedSimplex(const SheetedSimplex &ssimplex) {
		if (ws == nullptr)
			return;
		json j;
		j["type"] = "simplex";
		j["data"] = json::array();

		for (const Sheeted<D, size_t> &sp : ssimplex) {
			Point<D> p = castPoint(sp);
			j["data"].push_back(vector<double>(p.data(), p.data() + p.size()));
		}

		ws->send(j.dump());
		ws->poll();
	};
#endif // WSDEBUG
};


// Implements


template <typename K, typename V>
inline void toggle(map<K, V>& set, const K& key, const V& value) {
	pair<typename map<K, V>::const_iterator, bool>
		insert = set.insert({ key, value });
	if (!insert.second) {
		set.erase(insert.first);
	}
}
template <typename MAP>
inline void toggle(
	MAP& set, 
	const typename MAP::value_type& vpair
) {
	pair<typename MAP::const_iterator, bool>
		insert = set.insert(vpair);
	if (!insert.second) {
		set.erase(insert.first);
	}
}

template <unsigned D>
TorusDelaunay<D>::TorusDelaunay(shared_ptr<const vector<Point<D>>> points)
	: points(points), tkdtree(points) {
#ifdef WSDEBUG
	ws.reset(WebSocket::from_url(WSADDR));
	sendPoints();
#endif // WSDEBUG
#ifdef TIMERLOGGER
	timerlogger.reset();
#endif // TIMERLOGGER
	buildOnTorus();
}

template <unsigned D>
void TorusDelaunay<D>::build(
	const Box<D>& box,
	FaceMap& activeFaces) {
	if (box.type == Box<D>::POINT) {
		buildOnPointBox(box, activeFaces);
		return;
	}

	// box.type == Box<D>::BOX
	FaceMap activeCrossFaces;
	array<FaceMap, 2> activePureFaces;
	for (typename FaceMap::value_type& facepair : activeFaces) {
		classifyFaces(facepair, box.axis, box.wall, activeCrossFaces, activePureFaces);
	}

	while (!activeCrossFaces.empty()) {
		// pop
		typename FaceMap::value_type& crossfacepair = *activeCrossFaces.begin();
		// updateActiveFaces will delete crossface
		// activeCrossFaces.erase(activeCrossFaces.begin());

		Simplex&& newsimplex = Simplex();
		bool orientation = crossfacepair.second;  // orientation
		if (nextDelaunaySimplex(
			newsimplex, orientation,
			crossfacepair.first,  // face
			box
		)) {
			updateActiveFaces(
				newsimplex, orientation,
				box.axis, box.wall,
				activeCrossFaces,
				activePureFaces
			);
#ifdef WSDEBUG
			sendSimplex(newsimplex);
#endif // WSDEBUG
			pureSimplices.push_back(newsimplex);
		}
		else {
			// This is a boundary face
			// Should not happen to Torus
			cout << "WARN: Boundary Face is found!" << endl;
			activeCrossFaces.erase(activeCrossFaces.begin());
		}
	}
	// Divide
	build(box.subboxes[0], activePureFaces[0]);
	build(box.subboxes[1], activePureFaces[1]);
}

template<unsigned D>
void TorusDelaunay<D>::buildOnPointBox(
	const Box<D>& box,
	FaceMap& activeFaces
) {
	while (!activeFaces.empty()) {
		// pop
		typename FaceMap::value_type& facepair = *activeFaces.begin();
		// updateActiveFaces will delete face
		// activeFaces.erase(activeCrossFaces.begin());

		Simplex&& newsimplex = Simplex();
		bool orientation = facepair.second;  // orientation
		if (nextDelaunaySimplex(
			newsimplex, orientation,
			facepair.first,  // face
			box
		)) {
			// UpdateActiveFaces from the new simplex
			for (int i = 0; i <= D; ++i) {
				// flip orientation
				orientation = !orientation;
				Simplex face = Simplex(newsimplex);
				size_t prev_vertex = face[i];
				face.erase(face.begin() + i);
				toggle(activeFaces, face, orientation);
			}
#ifdef WSDEBUG
			sendSimplex(newsimplex);
#endif // WSDEBUG
			pureSimplices.push_back(newsimplex);
		}
		else {
			// This is a boundary face
			// Should not happen to Torus
			cout << "WARN: Boundary Face is found!" << endl;
			activeFaces.erase(activeFaces.begin());
		}
	}

	num_processed += box.size;
#ifdef TIMERLOGGER
	timerlogger << points->size() - num_processed << " points to process" << "\n";
#endif // TIMERLOGGER
}

template <unsigned D>
void TorusDelaunay<D>::buildOnTorus() {
	SheetedFaceMap activeCrossFaces;
	FaceMap activePureFaces;

	{
		// first simplex
		SheetedSimplex&& first = SheetedSimplex();
		if (findFirstOnBoundary(first)) {
			// sort the indices
			sort(
				first.begin(), first.end(), 
				[](auto &p1, auto &p2) {return p1.second < p2.second; }
			);
			// find orientation
			// orientation is INWARD if (p[0] - p[1]) * normal_of(p[1 to D]) > 0
			// mark INWARD as true for index orientation
			bool orientation = (normalOfFace<D>(
				simplexPoints(first.begin() + 1, first.end())
				).dot(castPoint(first[0]) - castPoint(first[1]))  > 0);

			updateActiveSheetedFaces(
				first, orientation,
				activeCrossFaces, activePureFaces
			);
			make_leading_zero<D>(first);
#ifdef WSDEBUG
			sendSheetedSimplex(first);
#endif // WSDEBUG
			crossSimplices.push_back(first);
		}
	}
	cout << "First simplex is found." << endl;
	unsigned int loop_count = 0;
	while (!activeCrossFaces.empty()) {
		++loop_count;
#ifdef TIMERLOGGER
		timerlogger << activeCrossFaces.size() << " faces to clear." << "\n";
#endif // TIMERLOGGER
		// pop
		typename SheetedFaceMap::value_type &crossfacepair = *activeCrossFaces.begin();
		// updateActiveSheetedFaces will delete crossface
		// activeCrossFaces.erase(activeCrossFaces.begin());

		SheetedSimplex&& newsimplex = SheetedSimplex();
		bool orientation = crossfacepair.second;
		if (nextSheetedDelaunaySimplex(
			newsimplex, orientation,
			crossfacepair.first // face
		)) {
			updateActiveSheetedFaces(
				newsimplex, orientation,
				activeCrossFaces, activePureFaces
			);
			make_leading_zero<D>(newsimplex);
#ifdef WSDEBUG
			sendSheetedSimplex(newsimplex);
#endif // WSDEBUG
			crossSimplices.push_back(newsimplex);
		}
		else {
			// This is a boundary face
			// Should not happen to Torus
			cout << "WARN: Boundary Face is found!" << endl;
			activeCrossFaces.erase(activeCrossFaces.begin());
		}
	}
	
	cout << "Boundary is finished!" << endl;
	num_processed = 0;
	cout << points->size() << " points in interior to process" << endl;
	build(tkdtree.KDTree<D>::rootBox, activePureFaces);
}

template <unsigned D>
void TorusDelaunay<D>::classifyFaces(
	const typename FaceMap::value_type &facepair,
	unsigned int axis, double wall,
	FaceMap &activeCrossFaces,
	array<FaceMap, 2> &activePureFaces
) {
	bool first = true, second = true;
	for (size_t i : facepair.first) {
		first &= ((*points)[i][axis] < wall);
		second &= ((*points)[i][axis] > wall);
		// == is treated as cross
	}

	if (first && (!second)) {
		toggle(activePureFaces[0], facepair);
	}
	else if (second && (!first)) {
		toggle(activePureFaces[1], facepair);
	}
	else {
		// !first && !second
		toggle(activeCrossFaces, facepair);
	}
}

template <unsigned D>
void TorusDelaunay<D>::classifySheetedFaces(
	const SheetedSimplex &face, bool orientation,
	SheetedFaceMap &activeCrossFaces,
	FaceMap &activePureFaces
) {
	Sheet<D> first = face[0].first;
	Simplex stripped(D);
	bool pure = true;
	for (int i = 0; i < D; ++i) {
		pair<Sheet<D>, size_t> p = face[i];
		pure &= (p.first == first);
		stripped[i] = p.second;
	}

	if (pure) {
		toggle(activePureFaces, stripped, orientation);
	}
	else {
		// !pure
		// find all translations
		SheetedFaceMap crossfaces;
		SheetedSimplex newface(D);
		for (int i = 0; i < D; ++i) {
			// translate by `first`
			first = face[i].first;
			for (int j = 0; j < D; ++j) {
				pair<Sheet<D>, size_t> p = face[j];
				newface[j] = make_pair(p.first - first, p.second);
			}
			// orientation is preserved by translation
			crossfaces.insert({ newface, orientation });
		}

		for (typename SheetedFaceMap::value_type &facepair : crossfaces) {
			toggle(activeCrossFaces, facepair);
		}
	}
}

template <unsigned D>
void TorusDelaunay<D>::updateActiveSheetedFaces(
	const SheetedSimplex &simplex,
	bool orientation,  // of the simplex
	SheetedFaceMap &activeCrossFaces,
	FaceMap &activePureFaces
) {
	for (int i = 0; i <= D; ++i) {
		// flip orientation
		orientation = !orientation;
		SheetedSimplex face = SheetedSimplex(simplex);
		Sheeted<D, size_t> prev_vertex = face[i];
		face.erase(face.begin() + i);

		classifySheetedFaces(
			face, orientation,
			activeCrossFaces, activePureFaces
		);
	}
}

template <unsigned D>
void TorusDelaunay<D>::updateActiveFaces(
	const Simplex &simplex,
	bool orientation,  // of the simplex
	unsigned int axis, double wall,
	FaceMap &activeCrossFaces,
	array<FaceMap, 2> &activePureFaces
) {
	for (int i = 0; i <= D; ++i) {
		// flip orientation
		orientation = !orientation;
		Simplex face = Simplex(simplex);
		size_t prev_vertex = face[i];
		face.erase(face.begin() + i);

		classifyFaces(
			{ face, orientation }, 
			axis, wall, 
			activeCrossFaces, activePureFaces
		);
	}
}

template <unsigned D>
bool TorusDelaunay<D>::findFirstOnWall(Simplex& ret, const Box<D>& box) {
	assert(box.type == Box<D>::BOX);
	ret.clear();

	// find the initial 2 points across the wall
	size_t id1, id2, id0;

	// find the first interior point
	if (!tkdtree.KDTree<D>::query(
		id1, NearestNeighbor<D>(Point<D>::Zero()), box.subboxes[0]
	)) {
		// cannot find a single point
		// Should not happen
		cout << "ERROR First point on left is missing!!" << endl;
		return false;
	}
	while (true) {
		if (!tkdtree.KDTree<D>::query(
			id2, NearestNeighbor<D>((*points)[id1]), box.subboxes[1]
		)) {
			cout << "ERROR Cannot find a point on right!" << endl;
			return false;
		}

		if (!tkdtree.KDTree<D>::query(
			id0, NearestNeighbor<D>((*points)[id2]), box.subboxes[0]
		)) {
			cout << "ERROR Cannot find a point on left from right!" << endl;
			return false;
		}

		if (id0 == id1) {
			// found
			break;
		}

		id1 = id0;
	}
	ret.push_back(id1);
	ret.push_back(id2);

	for (int d = 1; d < D; ++d) {
		// constructing (d+1)-dim simplex
		if (!growSimplex(ret, box)) {
			// Failed to find (d+1)-dim simplex
			cout << "ERROR Failed at " << d << " dim simplex on wall." << endl;
			return false;
		}
	}

	return true;
}

template <unsigned D>
bool TorusDelaunay<D>::findFirstOnBoundary(SheetedSimplex& ret) {
	ret.clear();

	// find the initial 2 points across the boundary
	Sheeted<D, size_t> &&ext = Sheeted<D, size_t>(), tmp;
	size_t intId;
	// find the first interior point
	if (!tkdtree.KDTree<D>::query(
		intId, NearestNeighbor<D>(Point<D>::Zero())
	)) {
		// cannot find a single point
		// Should not happen
		cout << "ERROR First point is missing!!" << endl;
		return false;
	}
	while (true) {
		if (!tkdtree.torusQuery(
			ext, NearestNeighborAcrossSheets<D>((*points)[intId])
		)) {
			// cannot find an exterior point
			cout << "ERROR Cannot find an exterior point!" << endl;
			return false;
		}

		if (!tkdtree.torusQuery(
			tmp, NearestNeighborAcrossSheets<D>((*points)[ext.second])
		)) {
			// cannot find an interior point
			cout << "ERROR Cannot find an interior point from exterior!" << endl;
			return false;
		}

		if ((tmp.second == intId) && (tmp.first == -ext.first)) {
			// found
			break;
		}

		intId = tmp.second;
	}
	ret.push_back(make_pair(Sheet<D>::Zero(), intId));
	ret.push_back(ext);

	for (int d = 1; d < D; ++d) {
		// constructing (d+1)-dim simplex
		if (!growSheetedSimplex(ret)) {
			// Failed to find (d+1)-dim simplex
			cout << "ERROR Failed at " << d << " dim simplex on boundary." << endl;
			return false;
		}
	}

	return true;
}

template <unsigned D>
inline bool TorusDelaunay<D>::growSimplex(Simplex &ret, const Box<D>& box) {
	SmallestSphere<D> sspredictor = SmallestSphere<D>(simplexPoints(ret));
	size_t nid;

	if (tkdtree.KDTree<D>::query(nid, sspredictor, box)) {
		// found
		// no need to sort
		ret.push_back(nid);
		return true;
	}
	else {
		// not found
		return false;
	}
}

template<unsigned D>
inline bool TorusDelaunay<D>::growSheetedSimplex(SheetedSimplex &ret) {
	SheetedSmallestSphere<D> sspredictor = SheetedSmallestSphere<D>(simplexPoints(ret));
	Sheeted<D, size_t> snid;

	if (tkdtree.torusQuery(snid, sspredictor)) {
		// found
		// no need to sort
		ret.push_back(snid);
		return true;
	}
	else {
		// not found
		return false;
	}
}

template <unsigned D>
bool TorusDelaunay<D>::nextDelaunaySimplex(
	Simplex &ret,
	bool &orientation,
	const Simplex& face,
	const Box<D>& root
) {
	DelaunaySphere<D> dspredictor = DelaunaySphere<D>(
		simplexPoints(face), orientation
	);
	size_t nid = 0;

	if (tkdtree.KDTree<D>::query(nid, dspredictor, root)) {
		// found
		ret = face;
		size_t pos = sorted_insert(ret, nid);
		if (pos % 2 == 1) {
			// flip orientation
			orientation = !orientation;
		}
		return true;
	}
	else {
		// not found
		return false;
	}
}

template <unsigned D>
bool TorusDelaunay<D>::nextSheetedDelaunaySimplex(
	SheetedSimplex &ret,
	bool &orientation,
	const SheetedSimplex& face
) {
	SheetedDelaunaySphere<D> dspredictor = SheetedDelaunaySphere<D>(
		simplexPoints(face), orientation
	);
	Sheeted<D, size_t> snid;
	if (tkdtree.torusQuery(snid, dspredictor)) {
		// found
		ret = face;
		size_t pos = sorted_insert(
			ret, snid, [](auto &p1, auto &p2) {return p1.second < p2.second; }
		);
		if (pos % 2 == 1) {
			// flip orientation
			orientation = !orientation;
		}
		return true;
	}
	else {
		// not found
		return false;
	}
}

