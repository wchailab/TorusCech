/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include "H5Cpp.h"

#include <string>
#include <vector>

#include "Geometry.h"
#include "TorusGeometry.h"
#include "BoundaryMap.h"

using namespace std;

// group names
const string DIM_K("/dim-");
const string PERSISTENT_K("/persistent-");
const string ECHELON("/echelon");

// datasets names
const string POINTS("/points");
const string SIMPLICES("/simplices");
const string SHEETS("/sheets");
const string VALUES("/values");
const string CRITICAL("/critical");
const string INTERIOR("/interior");
const string BOUNDARY_K("/boundary-");
const string BARS("/bars");
const string NEW_CYCLES("/new_cycles");
const string SP_INDICES("/indices");
const string SP_INDPTR("/indptr");

class H5Writer {
public:
	H5Writer(const string& fullpath) :
		file(fullpath, H5F_ACC_TRUNC) {
	};

	void write(
		const void* buf,
		const vector<size_t>& dim,
		const H5::PredType& pred_type,
		const string& group_name,
		const string& dataset_name
	) {
		try {
			H5::DataSpace dataspace(dim.size(), dim.data());
			H5::PredType datatype(pred_type);

			if (!group_name.empty() && !file.exists(group_name)) {
				file.createGroup(group_name);
			}

			H5::DataSet dataset = file.createDataSet(
				group_name + dataset_name, datatype, dataspace
			);
			dataset.write(buf, pred_type);

			file.flush(H5F_SCOPE_GLOBAL);
		}
		catch (H5::FileIException error) {
			error.printError();
			file.close();
			throw error;
		}
	};

private:
	H5::H5File file;
};


template <unsigned D>
class Saver {
public:
	Saver(const string& path, const string& filename)
		: fullpath(path + filename), writer(fullpath) {};
	
	void savePoints(const vector<Point<D>>& points) {
		vector<double> buffer;
		buffer.reserve(D * points.size());
		for (const Point<D> &p : points) {
			buffer.insert(buffer.end(), p.data(), p.data() + D);
		}
		assert(buffer.size() == D * points.size());

		writer.write(
			buffer.data(),
			{ points.size(), D }, 
			H5::PredType::NATIVE_DOUBLE,
			"", POINTS
		);
	};

	void saveSimplexValues(const SimplexValues<D>& simplexvalues) {
		{
			// for simplices and sheets
			vector<unsigned int> simplexbf;
			vector<int> sheetbf;
			simplexbf.reserve(
				(simplexvalues.dimension + 1) * simplexvalues.simplices.size()
			);
			sheetbf.reserve(
				D * (simplexvalues.dimension + 1) * simplexvalues.simplices.size()
			);

			for (const vector<Sheeted<D, size_t>>& simplex : simplexvalues.simplices) {
				for (const Sheeted<D, size_t>& sp : simplex) {
					simplexbf.push_back(sp.second);
					sheetbf.insert(
						sheetbf.end(), sp.first.data(), sp.first.data() + D
					);
				}
			}
			assert(
				simplexbf.size() == 
				(simplexvalues.dimension + 1) * simplexvalues.simplices.size()
			);
			assert(
				sheetbf.size() == 
				D * (simplexvalues.dimension + 1) * simplexvalues.simplices.size()
			);
			writer.write(
				simplexbf.data(),
				{
					simplexvalues.simplices.size(),
					simplexvalues.dimension + 1
				},
				H5::PredType::NATIVE_UINT,
				DIM_K + to_string(simplexvalues.dimension), SIMPLICES
				);
			writer.write(
				sheetbf.data(),
				{
					simplexvalues.simplices.size(),
					simplexvalues.dimension + 1,
					D
				},
				H5::PredType::NATIVE_INT,
				DIM_K + to_string(simplexvalues.dimension), SHEETS
				);
		}
		writer.write(
			simplexvalues.values.data(),
			{
				simplexvalues.values.size()
			},
			H5::PredType::NATIVE_DOUBLE,
			DIM_K + to_string(simplexvalues.dimension), VALUES
			);
		writer.write(
			simplexvalues.critical.data(),
			{
				simplexvalues.critical.size()
			},
			H5::PredType::NATIVE_UCHAR,
			DIM_K + to_string(simplexvalues.dimension), CRITICAL
			);
		writer.write(
			simplexvalues.interior.data(),
			{
				simplexvalues.interior.size()
			},
			H5::PredType::NATIVE_UCHAR,
			DIM_K + to_string(simplexvalues.dimension), INTERIOR
		);
	}

	void saveBoundary(const vector<vector<size_t>>& boundary, unsigned int k) {
		vector<unsigned int> buffer;
		buffer.reserve((k + 1) * boundary.size());
		for (const vector<size_t>& col : boundary) {
			buffer.insert(buffer.end(), col.begin(), col.end());
		}
		assert(
			buffer.size() == (k + 1) * boundary.size()
		);
		writer.write(
			buffer.data(),
			{
				boundary.size(),
				k + 1
			},
			H5::PredType::NATIVE_UINT,
			"", BOUNDARY_K + to_string(k)
			);
	}

	void savePersistent(
		const vector<array<size_t, 2>>& bars,
		const vector<size_t>& newCycles,
		const vector<vector<size_t>>& echelon,
		unsigned int k
	) {
		vector<unsigned int> barsbf;
		barsbf.reserve(2 * bars.size());
		for (const array<size_t, 2>& b : bars) {
			barsbf.push_back(b[0]);
			barsbf.push_back(b[1]);
		}
		writer.write(
			barsbf.data(),
			{
				bars.size(),
				2
			},
			H5::PredType::NATIVE_UINT,
			PERSISTENT_K + to_string(k), BARS
			);

		vector<unsigned int> newcbf(newCycles.begin(), newCycles.end());
		writer.write(
			newcbf.data(),
			{
				newcbf.size()
			},
			H5::PredType::NATIVE_UINT,
			PERSISTENT_K + to_string(k), NEW_CYCLES
			);

		// Column compress form of the sparse matrix:
		vector<unsigned int> indices;
		vector<unsigned int> indptr;
		unsigned int ptr = 0;
		for (const vector<size_t>& col : echelon) {
			indptr.push_back(ptr);
			indices.insert(indices.end(), col.begin(), col.end());
			ptr += col.size();
		}
		writer.write(
			indices.data(),
			{
				indices.size()
			},
			H5::PredType::NATIVE_UINT,
			PERSISTENT_K + to_string(k) + ECHELON, SP_INDICES
			);
		writer.write(
			indptr.data(),
			{
				indptr.size()
			},
			H5::PredType::NATIVE_UINT,
			PERSISTENT_K + to_string(k) + ECHELON, SP_INDPTR
		);
	}
private:
	string fullpath;
	H5Writer writer;
};


