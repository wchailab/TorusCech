/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include "KDTree.h"
#include "TorusGeometry.h"
#include <utility>
#include <queue>


template <unsigned D>
struct SheetedQueueData : public QueueData<D> {
	Sheet<D> sheet;

	SheetedQueueData(double value, const Sheet<D> &sheet, const Box<D> *box)
		: sheet(sheet), QueueData<D>(value, box) {
	};
	SheetedQueueData(double value, const Sheet<D> &sheet, size_t index)
		: sheet(sheet), QueueData<D>(value, index) {
	};
};

template <unsigned D>
class SheetedPredictor{
public:
	virtual double testBox(const Sheet<D>& sheet, const Box<D>& box) = 0;
	virtual double testPoint(const Sheet<D>& sheet, const Point<D>& point) = 0;
};


template <unsigned D>
class TorusKDTree : public KDTree<D> {
public:
	TorusKDTree(shared_ptr<const vector<Point<D>>> points);
	
	bool torusQuery(Sheeted<D, size_t>& ret, SheetedPredictor<D> &predictor) const;
	
	vector<Sheet<D>> sheets;

};


// Implements


template<unsigned D>
inline TorusKDTree<D>::TorusKDTree(shared_ptr<const vector<Point<D>>> points) 
	: KDTree<D>(points) {
	// Prebuild sheets
	Sheet<D> sheet = Sheet<D>::Constant(-1);
	bool loop = true;
	while (loop) {
		sheets.push_back(sheet);

		// increment
		for (size_t i = 0; i < D; ++i) {
			if (sheet(i) < 1) {
				sheet(i) += 1;
				break;
			}
			else {
				if (i < D - 1) {
					sheet(i) = -1;
					// next dimension
				}
				else {
					// the end
					loop = false;
					break;
				}
			}
		}
	}
}

template <unsigned D>
bool TorusKDTree<D>::torusQuery(
	Sheeted<D, size_t>& ret,
	SheetedPredictor<D> &predictor
) const {
	priority_queue<SheetedQueueData<D>> queue;

	// Initialize queue with all sheets
	for (const Sheet<D> &sheet : sheets) {
		double value = predictor.testBox(sheet, rootBox);
		if (!isnan(value))
			queue.push(SheetedQueueData<D>(
				value, sheet, &rootBox
				));
	}

	while (!queue.empty()) {
		SheetedQueueData<D> item = queue.top();
		queue.pop();

		if (item.type == SheetedQueueData<D>::POINT) {
			// a minimun is found
			ret = make_pair(item.sheet, item.index);
			return true;
		}

		// item.type == QueueData<D>::BOX:
		const Box<D>& subbox = *item.box;
		switch (subbox.type) {
		case Box<D>::BOX:
			for (const Box<D> &box : subbox.subboxes) {
				// discard if value is NaN
				double value = predictor.testBox(item.sheet, box);
				if (!isnan(value))
					queue.push(SheetedQueueData<D>(
						value, item.sheet, &box
					));
			}
			break;
		case Box<D>::POINT:
			for (size_t i : subbox.ptIndices) {
				// discard if value is NaN
				double value = predictor.testPoint(item.sheet, (*points)[i]);
				if (!isnan(value))
					queue.push(SheetedQueueData<D>(
						value, item.sheet, i
					));
			}
			break;
		default:
			break;
		}
	} // END of while !queue.empty()

	  // Did not find.
	return false;
};
