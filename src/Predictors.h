/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include "Geometry.h"
#include "TorusGeometry.h"
#include "KDTree.h"
#include "TorusKDTree.h"
#include <algorithm>

const double EPS = 1e-10;


template <unsigned D>
class NearestNeighbor : public Predictor<D> {
public:
	NearestNeighbor(const Point<D>& point) : p0(point) {};

	const Point<D>& p0;
	
	double testBox (const Box<D>& box) override {
		return distToBox2<D>(p0, box.vertices);
	};

	double testPoint(const Point<D>& point) override {
		return dist2<D>(p0, point);
	};
};

template <unsigned D>
class NearestNeighborOnTorus : public SheetedPredictor<D> {
public:
	NearestNeighborOnTorus(const Point<D>& point) : p0(point) {};

	const Point<D>& p0;

	double testBox(const Sheet<D>& sheet, const Box<D>& box) override {
		return distToBox2<D>(p0 - sheet.cast<double>(), box.vertices);
	};

	double testPoint(const Sheet<D>& sheet, const Point<D>& point) override {
		return dist2<D>(p0 - sheet.cast<double>(), point);
	};
};

template <unsigned D>
class NearestNeighborAcrossSheets : public SheetedPredictor<D> {
public:
	NearestNeighborAcrossSheets(const Point<D>& point) : p0(point) {};

	const Point<D>& p0;

	double testBox(const Sheet<D>& sheet, const Box<D>& box) override {
		if (sheet.isZero()) {
			return NAN;
		}
		return distToBox2<D>(p0 - sheet.cast<double>(), box.vertices);
	};

	double testPoint(const Sheet<D>& sheet, const Point<D>& point) override {
		if (sheet.isZero()) {
			return NAN;
		}
		return dist2<D>(p0 - sheet.cast<double>(), point);
	};
};

template <unsigned D>
class SmallestSphere : public Predictor<D> {
	// AKA: For D points, *double-sided* Delaunay sphere,
	// when the central sphere contains not point
public:
	SmallestSphere(const vector<Point<D>>&& face) 
		: face(face) {
		pair<double, Point<D>> p = radius_center<D>(face);
		r = p.first;
		center = p.second;
	};

	const vector<Point<D>> face;  // make a copy
	Point<D> center;
	double r;

	double testBox(const Box<D>& box) override {
		double R = sqrt(distToBox2<D>(center, box.vertices));
		return (R <= r) ? r : (r * r + R * R) / (2 * R);
	};

	double testPoint(const Point<D>& point) override {
		if (find(face.begin(), face.end(), point) != face.end()) {
			// coincide
			return NAN;
		}

		vector<Point<D>> newface = vector<Point<D>>(face);
		newface.push_back(point);
		return radius<D>(newface);
	};
};

template <unsigned D>
class SheetedSmallestSphere : public SheetedPredictor<D> {
public:
	SheetedSmallestSphere(const vector<Point<D>>&& face) : face(face) {
		// Assume the points in face are shifted in each sheet
		pair<double, Point<D>> p = radius_center<D>(face);
		r = p.first;
		center = p.second;
	};

	const vector<Point<D>> face;  // make a copy
	Point<D> center;
	double r;

	double testBox(const Sheet<D>& sheet, const Box<D>& box) override {
		double R = sqrt(distToBox2<D>(center - sheet.cast<double>(), box.vertices));
		return (R <= r)? r : (r * r + R * R) / (2 * R);
	};

	double testPoint(const Sheet<D>& sheet, const Point<D>& point) override {
		Point<D> spoint = point + sheet.cast<double>();
		if (find(face.begin(), face.end(), spoint) != face.end()) {
			// coincide
			return NAN;
		}
		vector<Point<D>> newface = vector<Point<D>>(face);
		newface.push_back(spoint);
		return radius<D>(newface);
	};
};
// AKA: *single-sided* Delaunay sphere
// Find the sphere whose interior contains no points
// value ~ the (directed) distance of the new center to the old center
template <unsigned D>
class DelaunaySphere : public Predictor<D> {
public:
	// orientation is true if new point * normal > 1
	DelaunaySphere(const vector<Point<D>>& face, bool orientation) 
		: face(face),
		normal(normalOfFace<D>(face, orientation)), 
		normalLen(normal.norm()) {
		pair<double, Point<D>> p = radius_center<D>(face);
		r = p.first;
		center = p.second;
	};

	double testBox(const Box<D>& box) override {
		double R = sqrt(distToBox2<D>(center, box.vertices));
		return _lowerBound(R);
	};

	double testPoint(const Point<D>& point) override {
		double dotprod = normal.dot(point - center);
		if (dotprod <= EPS) {
			// wrong orientation (<0) or linear independence (=0)
			return NAN;
		}
		return ((point - center).squaredNorm() - r * r) / dotprod;
	};

protected:
	const vector<Point<D>>& face;
	Point<D> center;
	double r;

	// normal * new_point > 1 if orientation
	Point<D> normal;
	double normalLen;

	inline double _lowerBound(const double& R) {
		return  (R < r) ? -INFINITY : (R * R - r * r) / (R * normalLen);
	}
};

// AKA: *single-sided* Delaunay sphere
// Find the sphere whose interior contains no points
// value ~ the (directed) distance of the new center to the old center
template <unsigned D>
class SheetedDelaunaySphere
	: protected DelaunaySphere<D>, public SheetedPredictor<D> {
public:
	// orientation is true if new point * normal > 1
	SheetedDelaunaySphere(const vector<Point<D>>& liftedFace, bool orientation)
		: DelaunaySphere<D>(liftedFace, orientation) {};

	double testBox(const Sheet<D> &sheet, const Box<D>& box) override {
		double R = sqrt(distToBox2<D>(
			center - sheet.cast<double>(),
			box.vertices
		));
		return DelaunaySphere<D>::_lowerBound(R);
	};

	double testPoint(const Sheet<D> &sheet, const Point<D>& point) override {
		return DelaunaySphere<D>::testPoint(point + sheet.cast<double>());
	};
};