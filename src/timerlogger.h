/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include <iostream>
#include <ctime>

using std::cerr;
using std::time;
using std::time_t;


class SwitchLogger {
public:
	SwitchLogger(bool logging) : logging(logging) {};

	// WARN! Does not support `endl`
	template <typename T>
	const SwitchLogger& operator<< (const T& arg) const {
		if (logging) {
			cerr << arg;
		}
		return *this;
	}
private:
	bool logging;
};

class TimerLogger {
public:
	TimerLogger(unsigned int delay) 
		: delay(delay), start(time(nullptr)), last(start) {};
	~TimerLogger() {};

	time_t start;
	time_t last;
	unsigned int delay;
	
	void reset() {
		start = time(nullptr);
	}

	// WARN! Does not support `endl`
	template <typename T>
	const SwitchLogger& operator<< (const T& arg) {
		time_t now = time(nullptr);
		if (now >= last + delay) {
			cerr << (now - start) << "s: " << arg;
			last = now;
			return TimerLogger::enabled;
		}
		else {
			return TimerLogger::disabled;
		}
	}

	static const SwitchLogger enabled;
	static const SwitchLogger disabled;
};