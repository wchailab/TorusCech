/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once

#include <cstdlib>
#include <array>
#include <utility>
#include <Eigen\Eigen>

using namespace std;
using namespace Eigen;

template <unsigned D> using Point = Matrix<double, D, 1>;


template <unsigned D>
inline double dist2(const Point<D> &p1, const Point<D> &p2) {
	return (p1 - p2).squaredNorm();
}

inline double distToInterval(double x, const array<double, 2> &interval) {
	if (x <= interval[0]) {
		return interval[0] - x;
	}
	else if (x >= interval[1]) {
		return x - interval[1];
	}
	else {
		// in the interval
		return 0.0;
	}
}

template <unsigned D>
inline double distToBox2(const Point<D> &x, const array<array<double, 2>, D> &box) {
	double sum = 0;
	for (int i = 0; i < D; ++i) {
		sum += pow(distToInterval(x[i], box[i]), 2);
	}
	return sum;
}

template <unsigned D>
inline VectorXd _radius(const vector<Point<D>> &pts) {
	size_t K = pts.size();

	MatrixXd mat(K + 1, K + 1);
	mat(0, 0) = 0;
	mat.block(1, 0, K, 1).fill(1);
	mat.block(0, 1, 1, K).fill(1);
	for (unsigned int i = 0; i < K; ++i) {
		mat(i + 1, i + 1) = 0;
		for (unsigned int j = 0; j < i; ++j) {
			double tmp = -(pts[i] - pts[j]).squaredNorm() / 2;
			mat(i + 1, j + 1) = tmp;
			mat(j + 1, i + 1) = tmp;
		}
	}

	VectorXd b = VectorXd::Zero(K + 1);
	b(0) = 1;
	return mat.colPivHouseholderQr().solve(b);
}

template <unsigned D>
pair<double, Point<D>> radius_center(const vector<Point<D>> &pts) {
	size_t K = pts.size();
	VectorXd sol = _radius<D>(pts);

	Point<D> center = Point<D>::Zero();
	for (unsigned int i = 0; i < K; ++i) {
		center += sol[i + 1] * pts[i];
	}
	return make_pair(sqrt(sol[0]), center);
}

template <unsigned D>
pair<double, Point<D>> radius_center_interior(
	const vector<Point<D>> &pts, bool &interior
) {
	size_t K = pts.size();
	VectorXd sol = _radius<D>(pts);

	Point<D> center = Point<D>::Zero();
	interior = true;
	for (unsigned int i = 0; i < K; ++i) {
		center += sol[i + 1] * pts[i];
		interior &= (sol[i + 1] >= 0);
	}
	return make_pair(sqrt(sol[0]), center);
}

template <unsigned D>
double radius(const vector<Point<D>> &pts) {
	VectorXd sol = _radius<D>(pts);
	return sqrt(sol[0]);
}


template <unsigned D, unsigned K>
pair<double, Point<D>> radiusK(const vector<Point<D>> &pts) {
	Matrix<double, K + 1, K + 1> mat;
	mat(0, 0) = 0;
	mat.block<K, 1>(1, 0).fill(1);
	mat.block<1, K>(0, 1).fill(1);
	for (unsigned int i = 0; i < K; ++i) {
		mat(i + 1, i + 1) = 0;
		for (unsigned int j = 0; j < i; ++j) {
			double tmp = -(pts[i] - pts[j]).squaredNorm() / 2;
			mat(i + 1, j + 1) = tmp;
			mat(j + 1, i + 1) = tmp;
		}
	}

	Matrix<double, K + 1, 1> b = Matrix<double, K + 1, 1>::Zero();
	b(0) = 1;

	Matrix<double, K + 1, 1> sol = mat.colPivHouseholderQr().solve(b);

	Point<D> center = Point<D>::Zero();
	for (unsigned int i = 0; i < K; ++i) {
		center += sol[i + 1] * pts[i];
	}
	return make_pair(sqrt(sol[0]), center);
}


template <unsigned D>
Point<D> normalOfFace(
	const vector<Point<D>>& face, 
	bool orientaition=true  // match with or against index orientation
) {
	// find the norm
	Matrix<double, D, D> mat;
	for (int i = 0; i < D; ++i) {
		mat.col(i) = face[i];
	}
	// ASSUME linear independence
	FullPivLU<Matrix<double, D, D>> lu(mat.transpose());

	// index orientation matches origin orientation if det < 0
	if (orientaition ^ (lu.determinant() > 0)) {
		return lu.solve(Point<D>::Ones());
	}
	else {
		return -lu.solve(Point<D>::Ones());
	}
}
