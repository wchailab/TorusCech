/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#include <vector>
#include <algorithm>

using namespace std;

template <typename T>
size_t sorted_insert(std::vector<T>& v, T elem) {
	// find the position for the element
	auto pos = std::upper_bound(v.begin(), v.end(), elem);
	size_t index = pos - v.begin();
	v.insert(pos, elem);
	return index;
};

template <typename T, typename Compare>
size_t sorted_insert(std::vector<T>& v, T elem, Compare comp) {
	// find the position for the element
	auto pos = std::upper_bound(v.begin(), v.end(), elem, comp);
	size_t index = pos - v.begin();
	v.insert(pos, elem);
	return index;
};