/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once

#include <vector>
#include <array>
#include <memory>
#include <queue>
#include <algorithm>
#include <cmath>

#include "Geometry.h"

template <unsigned D> struct Box;

template <unsigned D>
struct Box {
	array<array<double, 2>, D> vertices;
	size_t size = 0;
	enum {NIL, BOX, POINT} type;
	vector<Box<D>> subboxes;
	vector<size_t> ptIndices;

	// wall
	unsigned int axis;
	double wall;

	Box(): type(NIL) {};
	~Box() {};
};

template <unsigned D>
struct QueueData {
	double value;
	enum {BOX, POINT} type;
	union {
		const Box<D> *box;
		size_t index;
	};

	QueueData(double value, const Box<D> *box) 
		: value(value), box(box), type(BOX) {};
	QueueData(double value, size_t index)
		: value(value), index(index), type(POINT) {};

	bool operator <(const QueueData<D>& rhs) const {
		return value > rhs.value;
	};
};

template <unsigned D>
class Predictor {
public:
	virtual double testBox(const Box<D>& box) = 0;
	virtual double testPoint(const Point<D>& point) = 0;
};

template <unsigned D>
class KDTree {
public:
	KDTree(shared_ptr<const vector<Point<D>>> points);
	~KDTree() {};

	bool query(
		size_t& ret, 
		Predictor<D> &predictor,
		const Box<D>& root
	) const;
	inline bool query(size_t& ret, Predictor<D> &predictor) const {
		return query(ret, predictor, rootBox);
	};
	Box<D> rootBox;
	const shared_ptr<const vector<Point<D>>> points;

protected:
	void fillSubbox(Box<D>& root, vector<size_t>& indices, unsigned int axis=0);
};


// Implements


template <unsigned D>
KDTree<D>::KDTree(shared_ptr<const vector<Point<D>>> points) : points(points) {
	vector<size_t> indices = vector<size_t>(points->size());
	for (size_t i = 0, s = points->size(); i < s; ++i)
		indices[i] = i;

	fillSubbox(rootBox, indices, 0);
}

template<unsigned D>
void KDTree<D>::fillSubbox(Box<D>& root, vector<size_t>& indices, unsigned int axis) {
	root.size = indices.size();
	for (size_t i = 0; i < D; ++i) {
		auto bound = minmax_element(
			indices.begin(), indices.end(), 
			[&](size_t p1, size_t p2) {
				return (*points)[p1][i] < (*points)[p2][i];
		});
		root.vertices[i][0] = (*points)[*bound.first][i];
		root.vertices[i][1] = (*points)[*bound.second][i];
	}

	// open box if points are few
	if (root.size <= D * 2) {
		root.type = Box<D>::POINT;
		root.ptIndices = vector<size_t>(std::move(indices));
		return;
	}

	sort(indices.begin(), indices.end(), [&](size_t i1, size_t i2) {
		return (*points)[i1][axis] < (*points)[i2][axis];
	});

	typename vector<size_t>::iterator
		middle = indices.begin() + root.size / 2;

	root.axis = axis;
	root.wall = ((*points)[*middle][axis] + (*points)[*(middle - 1)][axis]) / 2;
	root.type = Box<D>::BOX;
	root.subboxes = vector<Box<D>>(2);
	// rotate
	axis = (axis < D - 1) ? axis + 1 : 0;

	vector<size_t>
		firsthalf(indices.begin(), middle),
		secondhalf(middle, indices.end());

	fillSubbox(
		root.subboxes[0],
		vector<size_t>(indices.begin(), middle),
		axis
	);
	fillSubbox(
		root.subboxes[1],
		vector<size_t>(middle, indices.end()),
		axis
	);
}

template<unsigned D>
bool KDTree<D>::query(
	size_t& ret,
	Predictor<D> &predictor,
	const Box<D>& root
) const {
	priority_queue<QueueData<D>> queue;

	double value = predictor.testBox(root);
	if (!isnan(value))
		queue.push(QueueData<D>(value, &root));

	ret = 0;
	while (!queue.empty()) {
		QueueData<D> item = queue.top();
		queue.pop();
		
		if (item.type == QueueData<D>::POINT) {
			// a minimun is found
			ret = item.index;
			return true;
		}

		// item.type == QueueData<D>::BOX:
		const Box<D>& subbox = *item.box;
		switch (subbox.type) {
		case Box<D>::BOX:
			for (const Box<D> &box : subbox.subboxes) {
				double value = predictor.testBox(box);
				// discard if value is NaN
				if (!isnan(value))
					queue.push(QueueData<D>(value, &box));
			}
			break;
		case Box<D>::POINT:
			for (size_t i : subbox.ptIndices) {
				double value = predictor.testPoint((*points)[i]);
				// discard if value is NaN
				if (!isnan(value))
					queue.push(QueueData<D>(value, i));
			}
			break;
		default:
			break;
		}
	} // END of while !queue.empty()

	  // Did not find.
	return false;
}