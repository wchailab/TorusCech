/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#include "constants.h"

#include <iostream>
#include <memory>
#include <algorithm>
#include <utility>

#include "Geometry.h"
#include "TorusDelaunay.h"
#include "BoundaryMap.h"
#include "save.h"

using namespace std;
using namespace Eigen;


TorusDelaunay<DIM> testDelauney() {
	auto points = make_shared<vector<Point<DIM>>>();

	for (int i = 0; i < NUM_POINTS; ++i) {
		points->push_back((Point<DIM>::Random().array() + 1.) * .5);
	}
	cout << "Points are created." << endl;

	TorusDelaunay<DIM> delaunay(points);

	cout << "Pure: " << delaunay.pureSimplices.size() << endl;
	cout << "Cross: " << delaunay.crossSimplices.size() << endl;
	cout << endl;

	cout << "Testing delaunay..." << endl;
	
	bool failed = false;
	// test if top critical points are all critical
	for (size_t iface = 0, size = delaunay.pureSimplices.size(); iface < size; ++iface) {
		// make point face
		vector<Point<DIM>> ptface;
		for (const size_t p : delaunay.pureSimplices[iface]) {
			ptface.push_back((*points)[p]);
		}
		pair<double, Point<DIM>> rc = radius_center<DIM>(ptface);

		// test if critical
		size_t point;
		if (delaunay.tkdtree.query(point, NearestNeighbor<DIM>(rc.second))) {
			// test if other points are in sphere
			if (find(
				delaunay.pureSimplices[iface].begin(), 
				delaunay.pureSimplices[iface].end(), point
			) == delaunay.pureSimplices[iface].end()) {
				cout << "TEST FAILED! "
					<< "One of the pure simplices has other points in the sphere!"
					<< endl;
				cout << "\tnumber: " << iface << endl;
				failed = true;
				break;
			}
		}
	}
	if (!failed) {
		cout << "TEST PASSEDED for pure simplices." << endl;
	}
	failed = false;
	for (size_t iface = 0, size = delaunay.crossSimplices.size(); iface < size; ++iface) {
		// make point face
		vector<Point<DIM>> ptface;
		for (const Sheeted<DIM, size_t> &p : delaunay.crossSimplices[iface]) {
			ptface.push_back((*points)[p.second] + p.first.cast<double>());
		}
		pair<double, Point<DIM>> rc = radius_center<DIM>(ptface);

		// test if critical
		Sheeted<DIM, size_t> spoint;
		if (delaunay.tkdtree.torusQuery(spoint, NearestNeighborOnTorus<DIM>(rc.second))) {
			// test if other points are in sphere
			if (find(
				delaunay.crossSimplices[iface].begin(), 
				delaunay.crossSimplices[iface].end(), 
				spoint
			) == delaunay.crossSimplices[iface].end()) {
				cout << "TEST FAILED! "
					<< "One of the cross simplices has other points in the sphere!"
					<< endl;
				cout << "\tnumber: " << iface << endl;
				failed = true;
				break;
			}
		}
	}
	if (!failed) {
		cout << "TEST PASSEDED for cross simplices." << endl;
	}

	cout << "TEST for delaunay ended." << endl << endl;

	return delaunay;
}

unsigned int combinatorial(unsigned int n, unsigned int k) {
	if (k * 2 > n)
		k = n - k;
	if (k == 0)
		return 1;

	unsigned int ret = n;
	for (unsigned int i = 2; i <= k; ++i) {
		ret *= n - i + 1;
		ret /= i;
	}
	return ret;
}

// Test if the final betti numbers match. combinatorial(DIM, k)
void testHomology(
	TorusDelaunay<DIM>& delaunay, Saver<DIM> &saver
) {
	cout << "Testing homology and betti numbers..." << endl;

	SimplexValues<DIM> &&top = fromTopSimplex<DIM>(
		delaunay.tkdtree,
		delaunay.crossSimplices,
		delaunay.pureSimplices
		);

	cout << DIM << "-d has " << top.simplices.size() << " simplices." << endl;
	cout << "\twith " << count(
		top.critical.begin(), top.critical.end(), true
	) << " critical points" << endl;
	cout << "\twith " << count(
		top.interior.begin(), top.interior.end(), true
	) << " centers in the interior" << endl;
	auto bound = minmax_element(top.values.begin(), top.values.end());
	cout << "\trange: " << *bound.first << " -> " << *bound.second << endl;

	saver.saveSimplexValues(top);
	cout << DIM << "-d is saved." << endl;

	unsigned int higher_killers = 0;
	SimplexValues<DIM> parents = std::move(top);
	for (unsigned int k = DIM; k > 0; --k) {
		cout << "==========" << endl;
		// from k to (k - 1)
		BoundaryMap<DIM> bdry = BoundaryMap<DIM>(delaunay.tkdtree, parents);
		cout << "==========" << endl;

		cout << "Saving..." << endl;
		saver.saveSimplexValues(bdry.faces);
		saver.saveBoundary(bdry.boundary, bdry.faces.dimension + 1);
		saver.savePersistent(
			bdry.bars, bdry.newCycles, bdry.echelon, bdry.faces.dimension +1
		);
		cout << "Saved." << endl;

		// k-d data:
		cout << "\tcreators: " << bdry.newCycles.size() << endl;
		unsigned int true_betti = combinatorial(DIM, k);
		cout << "\tbetti_" << k << " = "
			<< bdry.newCycles.size() - higher_killers << endl;
		if (bdry.newCycles.size() - higher_killers != true_betti) {
			cout << "TEST FAILED! "
				<< k << "-betti does not match "
				<< true_betti << endl;
		}
		else {
			cout << "\tTEST PASSED! " << k << "-betti matches" << endl << endl;
		}

		cout << "\tTesting values" << endl;
		bool success = true;
		for (size_t iparent = 0; success && (iparent < parents.simplices.size()); ++iparent) {
			double pvalue = parents.values[iparent];
			for (size_t iface : bdry.boundary[iparent]) {
				if (pvalue < bdry.faces.values[iface]) {
					success = false;
					cout << "\tTEST FAILED! "
						<< "face greater than parent! ( "
						<< iparent << " : " << pvalue << ", "
						<< iface << " : " << bdry.faces.values[iface]
						<< " )" << endl;
					break;
				}
			}
		}
		if (success) {
			cout << "\tTEST PASSED! Parent values are larger." << endl;
		}

		// (k - 1)-d data:
		cout << (k - 1) << "-d has " << bdry.faces.simplices.size() << " simplices." << endl;
		cout << "\twith " << count(
			bdry.faces.critical.begin(), bdry.faces.critical.end(), true
		) << " critical points" << endl;
		cout << "\twith " << count(
			bdry.faces.interior.begin(), bdry.faces.interior.end(), true
		) << " centers in the interior" << endl;
		auto bound = minmax_element(bdry.faces.values.begin(), bdry.faces.values.end());
		cout << "\trange: " << *bound.first << " -> " << *bound.second << endl;

		// update for the next loop
		higher_killers = parents.simplices.size() - bdry.newCycles.size();
		parents = std::move(bdry.faces);
	}

	// 0-d data, i.e. points;
	// theoretical result since boundary is always 0
	cout << "\tcreators: " << parents.simplices.size() << endl;
	unsigned int true_betti = combinatorial(DIM, 0);
	cout << "\tbetti_0 = "
		<< parents.simplices.size() - higher_killers << endl;
	if (parents.simplices.size() - higher_killers != true_betti) {
		cout << "TEST FAILED! "
			<< "0-betti does not match "
			<< true_betti << endl;
	}
	else {
		cout << "\tTEST PASSED! 0-betti matches" << endl;
	}

	cout << "TEST for homology and betti numbers ended!" << endl;
}