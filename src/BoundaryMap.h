/***
    Copyright (C) 2018 Wei Chai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
***/
#pragma once
#include "Geometry.h"
#include "TorusGeometry.h"
#include "Predictors.h"
#include <iostream>
#include <utility>
#include <map>
#include <algorithm>
#include <iterator>

#ifdef TIMERLOGGER
#include "timerlogger.h"
extern TimerLogger timerlogger; // defined in timerlogger.cpp
#endif

// Assume vectors are sorted and no repetition occurs
template <typename T>
void xor_merge_sorted(const vector<T>& from, vector<T> &to, vector<T> &tmp) {
	tmp.clear();
	typename vector<T>::const_iterator fromIt = from.begin();
	typename vector<T>::const_iterator toIt = to.begin();
	typename vector<T>::const_iterator fromEnd = from.end();
	typename vector<T>::const_iterator toEnd = to.end();
	while (true) {
		if (fromIt >= fromEnd) {
			// from is exhausted
			tmp.insert(tmp.end(), toIt, toEnd);
			break;
		}
		if (toIt >= toEnd) {
			// to is exhausted
			tmp.insert(tmp.end(), fromIt, fromEnd);
			break;
		}
		// add the smaller item
		if (*fromIt < *toIt) {
			typename vector<T>::const_iterator end = lower_bound(fromIt, fromEnd, *toIt);
			tmp.insert(tmp.end(), fromIt, end);
			fromIt = end;
		}
		else if (*fromIt > *toIt) {
			typename vector<T>::const_iterator end = lower_bound(toIt, toEnd, *fromIt);
			tmp.insert(tmp.end(), toIt, end);
			toIt = end;
		}
		else {
			// *fromIt == *toIt
			++fromIt;
			++toIt;
			// do nothing since it's an __XOR__ merge
		}
	}

	to.swap(tmp);
}


template <unsigned D>
struct SimplexValues {
	using Simplex = vector<Sheeted<D, size_t>>;

	unsigned int dimension;
	vector<Simplex> simplices;
	vector<double> values;
	vector<unsigned char> critical;
	vector<unsigned char> interior;  // center is in the interior of simplex
};


template <unsigned D>
class BoundaryMap {
public:
	using Simplex = vector<Sheeted<D, size_t>>;

	BoundaryMap(
		const TorusKDTree<D> &tkdtree,
		const SimplexValues<D> &parents
	);
	~BoundaryMap() {};

	const SimplexValues<D> &parents;
	SimplexValues<D> faces;
	// sorted vector of boundary face ids of each parent simplex
	vector<vector<size_t>> boundary;

	// persistent homology
	// vector of (birth id in `faces`, death id in `parents`)
	vector<array<size_t, 2>> bars;
	// `D` dimensional new cycles in `parents`
	vector<size_t> newCycles;
	vector<vector<size_t>> echelon;
private:
	shared_ptr<const vector<Point<D>>> points;
	const TorusKDTree<D>& tkdtree;
	
	void makeBoundary();
	void columnReduction();

	double getValue(
		const Simplex& simplex, const vector<size_t>& parents,
		unsigned char &critical, unsigned char &interior
	);

	inline Point<D> castPoint(const Sheeted<D, size_t>& p) {
		return (*points)[p.second] + p.first.cast<double>();
	}
	inline vector<Point<D>> simplexPoints(const Simplex &simplex) {
		vector<Point<D>> ptface;
		for (const Sheeted<D, size_t> &p : simplex) {
			ptface.push_back(castPoint(p));
		}
		return ptface;
	};
};

template<unsigned D>
inline BoundaryMap<D>::BoundaryMap(
	const TorusKDTree<D> &tkdtree,
	const SimplexValues<D> &parents
) : tkdtree(tkdtree), points(tkdtree.KDTree<D>::points),
	parents(parents),
	boundary(parents.simplices.size()) 
{
	assert(parents.dimension >= 1);
	faces.dimension = parents.dimension - 1;
	cout << "Finding boundaries (dim=" 
		<< faces.dimension << ") and values..." << endl;
	makeBoundary();
	cout << "Finding persistent (dim=" 
		<< faces.dimension << ") homologies..." << endl;
	columnReduction();
}

template<unsigned D>
void BoundaryMap<D>::makeBoundary() {
	using ParentMap = map<Simplex, vector<size_t>, CompareSheet<D>>;
	ParentMap face_to_parents;

	for (size_t iparent = 0, parent_size = parents.simplices.size(); 
		iparent < parent_size; ++iparent) {
		// First face
		Simplex face = Simplex(parents.simplices[iparent]);
		Sheet<D> sheet = face[1].first;
		face.erase(face.begin());
		for (Sheeted<D, size_t> &point : face) {
			point.first -= sheet;
		}
		face_to_parents[face].push_back(iparent);

		// From second to end
		for (size_t iface = 1; iface <= parents.dimension; ++iface) {
			Simplex face = Simplex(parents.simplices[iparent]);
			face.erase(face.begin() + iface);
			face_to_parents[face].push_back(iparent);
		}
	}

	// calculate values
	size_t face_size = face_to_parents.size();
	vector<double> values;
	// sorted[i] = j <=> ith largest is face[j]
	vector<size_t> sorted;
	vector<unsigned char> critical;
	vector<unsigned char> interior;
	vector<Simplex> simplices;
	vector<vector<size_t>> vparents;
	values.reserve(face_size);
	sorted.reserve(face_size);
	critical.reserve(face_size);
	interior.reserve(face_size);
	simplices.reserve(face_size);
	vparents.reserve(face_size);
	{
		size_t i = 0;
		unsigned char c;
		unsigned char intr;
		for (pair<Simplex, vector<size_t>> p : face_to_parents) {
			values.push_back(getValue(p.first, p.second, c, intr));
			simplices.push_back(p.first);
			vparents.push_back(std::move(p.second));
			critical.push_back(c);
			interior.push_back(intr);
			sorted.push_back(i);
			++i;
#ifdef TIMERLOGGER
			timerlogger << (i + 1) << " / " << face_size << " faces were processed in "
				<< faces.dimension << "-d simplices." << "\n";
#endif // TIMERLOGGER
		}
	}
	if (faces.dimension > 0)
		sort(sorted.begin(), sorted.end(), [&](size_t i, size_t j) {
			return values[i] < values[j];
		});
	else // faces.dimension == 0; values are all 0; sort by index
		sort(sorted.begin(), sorted.end(), [&](size_t i, size_t j) {
		return simplices[i][0].second < simplices[j][0].second;
	});

	faces.simplices.clear();
	faces.values.clear();
	faces.critical.clear();
	faces.interior.clear();
	faces.simplices.reserve(face_size);
	faces.values.reserve(face_size);
	faces.critical.reserve(face_size);
	faces.interior.reserve(face_size);
	for (size_t iface = 0; iface < face_size; ++iface) {
		faces.critical.push_back(critical[sorted[iface]]);
		faces.interior.push_back(interior[sorted[iface]]);
		faces.values.push_back(values[sorted[iface]]);
		faces.simplices.push_back(std::move(simplices[sorted[iface]]));
		for (size_t iparent : vparents[sorted[iface]]) {
			boundary[iparent].push_back(iface);
		}
	}

	cout << "Values of " << faces.dimension << "-d simplices are calculated." << endl;
}

template<unsigned D>
inline void BoundaryMap<D>::columnReduction() {
	echelon.clear();
	echelon.insert(echelon.end(), boundary.begin(), boundary.end());
	size_t face_size = faces.simplices.size();
	size_t parent_size = echelon.size();
	vector<size_t> killedBy(face_size, parent_size);

	vector<size_t> tmp;
	for (size_t icol = 0; icol < parent_size; ++icol) {
		vector<size_t>& col = echelon[icol];
		while (true) {
			if (col.empty()) {
				// empty col => no boundary
				// => new cycle is formed by _parent_ simplices at `icol`
				newCycles.push_back(icol);
				break;
			}
			if (killedBy[col.back()] >= parent_size) {
				// a cycle created at `col.back()` is killed at `icol`
				bars.push_back({ col.back(), icol });
				killedBy[col.back()] = icol;
				break;
			}

			// XOR merge
			xor_merge_sorted(echelon[killedBy[col.back()]], col, tmp);
		}
		echelon[icol].shrink_to_fit();
#ifdef TIMERLOGGER
		timerlogger << (icol + 1) << " / " << parent_size 
			<< " columns were deducted." << "\n"
			<< "\t" << newCycles.size() << " zero columns " << "\n"
			<< "\tsize = " << echelon[icol].size() << " in " << icol
			<< "-th column after reduction" << "\n";
#endif // TIMERLOGGER
	}
}

template<unsigned D>
inline double BoundaryMap<D>::getValue(
	const Simplex& simplex, const vector<size_t>& parent,
	unsigned char &critical, unsigned char &interior
) {
	bool _intr;
	pair<double, Point<D>> rc = radius_center_interior<D>(
		simplexPoints(simplex), _intr
	);
	interior = _intr;
	Sheeted<D, size_t> spoint;
	if (tkdtree.torusQuery(spoint, NearestNeighborOnTorus<D>(rc.second))) {
		if (find(simplex.begin(), simplex.end(), spoint) != simplex.end()) {
			// no other point
			critical = true;
			return rc.first;  // radius
		}
		else {
			// other points may in sphere
			critical = false;
			size_t m = *min_element(
				parent.begin(), parent.end(), [&](size_t a, size_t b) {
				return parents.values[a] < parents.values[b];
			});
			return parents.values[m];
		}
	}
	else {
		// cannot find nearest neighbor
		// Should not happen
		cout << "ERROR Cannot find nearest neighbor!!" << endl;
		return NAN;
	}
}

template<unsigned D>
SimplexValues<D> fromTopSimplex(
	const TorusKDTree<D> &tkdtree, // TODO: points
	const vector<vector<Sheeted<D, size_t>>>& crossSimplices,
	const vector<vector<size_t>>& pureSimplices
) {
	size_t size = crossSimplices.size() + pureSimplices.size();

	SimplexValues<D> ret;
	ret.dimension = D;
	// Add simplices
	vector<vector<Sheeted<D, size_t>>>&& faces = vector<vector<Sheeted<D, size_t>>>();
	faces.reserve(size);
	copy(crossSimplices.begin(), crossSimplices.end(), back_inserter(faces));
	for (const vector<size_t>& pure : pureSimplices) {
		vector<Sheeted<D, size_t>> &&transformed = vector<Sheeted<D, size_t>>();
		transformed.reserve(D + 1);
		for (size_t p : pure) {
			transformed.push_back({ Sheet<D>::Zero(), p });
		}
		faces.emplace_back(std::move(transformed));
	}
	// Calculate the values
	vector<double> values;
	vector<unsigned char> critical;
	vector<unsigned char> interior;
	vector<size_t> sorted;
	values.reserve(size);
	critical.reserve(size);
	interior.reserve(size);
	sorted.reserve(size);
	for (int iface = 0; iface < size; ++iface) {
		sorted.push_back(iface);
		// make point face
		vector<Point<D>> ptface;
		for (const Sheeted<D, size_t> &p : faces[iface]) {
			ptface.push_back((*(tkdtree.points))[p.second] + p.first.cast<double>());
		}
		bool _interior = true;
		// get value
		pair<double, Point<D>> rc = radius_center_interior<D>(ptface, _interior);
		values.push_back(rc.first);
		interior.push_back(_interior);

		// test if critical
		Sheeted<D, size_t> spoint;
		if (tkdtree.torusQuery(spoint, NearestNeighborOnTorus<D>(rc.second))) {
			// test if other points are in sphere
			critical.push_back(
				find(faces[iface].begin(), faces[iface].end(), spoint)
				!= faces[iface].end()
			);
		}
		else {
			// cannot find nearest neighbor
			// Should not happen
			cout << "ERROR Cannot find nearest neighbor!!" << endl;
			// set critical to false; howerver, not making sense
			critical.push_back(false);
		}
#ifdef TIMERLOGGER
		timerlogger << iface << " / " << size << " faces to process in "
			<< D << "-d simplices." << "\n";
#endif // TIMERLOGGER
	}
	// sort
	sort(sorted.begin(), sorted.end(), [&](size_t i, size_t j) {
		return values[i] < values[j];
	});

	ret.simplices.clear();
	ret.values.clear();
	ret.critical.clear();
	ret.interior.clear();
	ret.simplices.reserve(size);
	ret.values.reserve(size);
	ret.critical.reserve(size);
	ret.interior.reserve(size);
	for (int iface = 0; iface < size; ++iface) {
		ret.critical.push_back(critical[sorted[iface]]);
		ret.interior.push_back(interior[sorted[iface]]);
		ret.values.push_back(values[sorted[iface]]);
		ret.simplices.push_back(std::move(faces[sorted[iface]]));
	}

	cout << "Values of " << D << "-d simplices are calculated." << endl;

	return ret;
}
