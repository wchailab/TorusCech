# [TorusCech](https://gitlab.com/wchailab/TorusCech)
## Computing persistent Čech homology on an `n`-D flat torus

* [`src`](https://gitlab.com/wchailab/TorusCech/blob/master/src/): C++ codes to do the computation
    - [`KDTree.h`](https://gitlab.com/wchailab/TorusCech/blob/master/src/KDTree.h): A general **kD tree** which applies to multiple types of queries.
    - `TorusKDTree.h`: An adaptation of kD tree to a flat torus.
    - `Predictors.h`: Classes for different types of queries in an kD tree. Each class has a function to evaluate at a single point as well as a prediction on a bounding box. Query types include nearest neighbors and more.
    - [`TorusDelaunay.h`](https://gitlab.com/wchailab/TorusCech/blob/master/src/TorusDelaunay.h): A **divide and conquer** algorithm to build Delaunay triangulation on a flat torus.
    - `BoundaryMap.h`: Construction of the Čech complex and the boundary maps based on Delaunay triangulation. Computation of the homology.
    - `Geometry.h`: Common geometric operations.

[![](https://beacon-c.appspot.com/gitlab/TorusCech/src?name)](https://gitlab.com/wchailab/TorusCech)